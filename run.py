import time
import re
from selenium import webdriver
from selenium.webdriver import FirefoxOptions
opts = FirefoxOptions()
opts.add_argument("--headless")
browser = webdriver.Firefox(firefox_options=opts)
searchPattern='window.DD.currentServiceProperties ='
browser.get('https://downdetector.ru/ne-rabotaet/rostelekom/')
html = browser.page_source
browser.close()
dataParse=re.findall('(<script(.+?)<\/script>)', html,flags=re.DOTALL)

id='id'
status='status'
max_baseline='max_baseline'
min_baseline='min_baseline'
communicate='communicate'
null='null'
company='company'
series='series'
reports='reports'
label='label'
data='data'
x='x'
y='y'
baseline='baseline'
max='max'

for i in dataParse:
    if searchPattern in i[0]:
        res=re.findall('(%s(.+?)*)' % searchPattern, i[1],flags=re.DOTALL)[0][0].replace("\n",'')
        res=eval(res.replace(searchPattern,''))
        break

reports_list=[]
for i in res['series']['reports']['data']:
    reports_list.append({'x':int(time.mktime(time.strptime(i['x'].split('+')[0] ,'%Y-%m-%dT%H:%M:%S'))),'y':i['y']})
reports_list=sorted(reports_list,key=lambda k: k['x'])

baseline_list=[]
for i in res['series']['baseline']['data']:
    baseline_list.append({'x':int(time.mktime(time.strptime(i['x'].split('+')[0] ,'%Y-%m-%dT%H:%M:%S'))),'y':i['y']})
baseline_list=sorted(baseline_list,key=lambda k: k['x'])

from pyzabbix import ZabbixMetric, ZabbixSender
ZSERVER_IP='127.0.0.1'
h_name='downdetector.ru'
packet = []
packet.append(ZabbixMetric(h_name, 'report', reports_list[-1]['y']))
packet.append(ZabbixMetric(h_name, 'baseline', baseline_list[-1]['y']))

zbx = ZabbixSender(ZSERVER_IP)
try:
    zbx.send(packet)
except:
    print('Данные в zabbix не отправлены')